<?php

namespace Tests\Feature;

use DB;
use Tests\TestCase;

class UrlQueryTest extends \Orchestra\Testbench\TestCase
{
    protected function getPackageProviders($app)
    {
        return ['SkillFactory\QueryFilter\QueryFilterServiceProvider'];
    }

    public function testCreatingQuery()
    {
        $query = DB::table('users');

        $this->assertEquals('select * from `users`', $query->toSql());
        $query->where('name', 'Damir');
        $this->assertEquals('select * from `users` where `name` = ?', $query->toSql());
    }

    public function testSearchBy()
    {
        $query = DB::table('users');
        $query->searchBy(['q_name' => 'something']);
        $this->assertEquals('select * from `users` where `name` like ?', $query->toSql());
        $this->assertEquals(['%something%'], $query->getBindings());
    }

    public function testSearchByWithTableName()
    {
        $query = DB::table('users');
        $query->searchBy(['q_users:name' => 'something']);
        $this->assertEquals('select * from `users` where `users`.`name` like ?', $query->toSql());
        $this->assertEquals(['%something%'], $query->getBindings());
    }

    public function testSearchByConvert()
    {
        $query = DB::table('users');
        $query->searchByConvert(['q_name' => 'something']);
        $this->assertEquals('select * from `users` where convert(lower(`name`) using utf8) like convert(lower(?) using utf8)', $query->toSql());
        $this->assertEquals(['%something%'], $query->getBindings());
    }

    public function testSearchByConvertJson()
    {
        $query = DB::table('users');
        $query->searchByConvert(['q_name->en' => 'something']);
        $this->assertEquals('select * from `users` where convert(lower(`name`->\'$."en"\') using utf8) like convert(lower(?) using utf8)', $query->toSql());
        $this->assertEquals(['%something%'], $query->getBindings());
    }

    public function testSearchByWithOtherParameters()
    {
        $query = DB::table('users');
        $query->where('age', '28');
        $query->searchBy(['q_name' => 'something', 'q_last_name' => 'other']);
        $this->assertEquals('select * from `users` where `age` = ? and `name` like ? and `last_name` like ?', $query->toSql());
        $this->assertEquals(['28', '%something%', '%other%'], $query->getBindings());
    }

    public function testOrderBy()
    {
        $query = DB::table('users');
        $query->sortBy(['by' => 'age', 'direction' => 'asc']);
        $this->assertEquals('select * from `users` order by `age` asc', $query->toSql());
    }

    public function testOrderByDirection()
    {
        $query = DB::table('users');
        $query->sortBy(['by' => 'age', 'direction' => 'nesto']);
        $this->assertEquals('select * from `users` order by `age` desc', $query->toSql());
    }

    public function testOrderByDirectionWithSpecificTable()
    {
        $query = DB::table('users');
        $query->sortBy(['by' => 'users:age', 'direction' => 'nesto']);
        $this->assertEquals('select * from `users` order by `users`.`age` desc', $query->toSql());
    }

    public function testMatchBy()
    {
        $query = DB::table('users');
        $query->matchBy(['m_age' => '21', 'm_category_id' => [4, 5]]);
        $this->assertEquals('select * from `users` where `age` = ? and `category_id` in (?, ?)', $query->toSql());
        $this->assertEquals(['21', '4', '5'], $query->getBindings());
    }

    public function testInRange()
    {
        $query = DB::table('users');
        $query->inRange(['r_date' => '2015-01-05--2018-12-31']);
        $this->assertEquals('select * from `users` where `date` between ? and ?', $query->toSql());
        $this->assertEquals(['2015-01-05', '2018-12-31'], $query->getBindings());

        $query = DB::table('users');
        $query->inRange(['r_date' => '--2018-12-31']);
        $this->assertEquals('select * from `users` where `date` <= ?', $query->toSql());
        $this->assertEquals(['2018-12-31'], $query->getBindings());

        $query = DB::table('users');
        $query->inRange(['r_date' => '2015-01-05--']);
        $this->assertEquals('select * from `users` where `date` > ?', $query->toSql());
        $this->assertEquals(['2015-01-05'], $query->getBindings());
    }

    public function testApplyAllFilters()
    {
        $query = DB::table('users');
        $query->applyQuery([
          'm_city' => 'Belgrade',
          'q_name' => 'something',
          'r_created_at' => '2018-01-01--2019-01-01',
          'by' => 'name',
          'direction' => 'asc',
        ]);
        $this->assertEquals('select * from `users` where `city` = ? and `created_at` between ? and ? and `name` like ? order by `name` asc', $query->toSql());
        $this->assertEquals(['Belgrade', '2018-01-01', '2019-01-01', '%something%'], $query->getBindings());
    }
}
