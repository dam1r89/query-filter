<?php

namespace SkillFactory\QueryFilter;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class QueryFilterServiceProvider extends ServiceProvider
{

    /**
     * Php does not support dots in request names, so we are using
     * colons to specify table name. Format <table_name>:<column>
     * @return String
     */
    public function unwrapColon($name)
    {
        return str_replace(':', '.', $name);
    }

    protected function isJsonSelector($value)
    {
        return Str::contains($value, '->');
    }
    public function wrapValue($value)
    {
        if ($value === '*') {
            return $value;
        }

        // If the given value is a JSON selector we will wrap it differently than a
        // traditional value. We will need to split this path and wrap each part
        // wrapped, etc. Otherwise, we will simply wrap the value as a string.
        if ($this->isJsonSelector($value)) {
            return $this->wrapJsonSelector($value);
        }

        return '`'.str_replace('`', '``', $value).'`';
    }

    protected function wrapJsonSelector($value)
    {
        $path = explode('->', $value);

        $field = $this->wrapValue(array_shift($path));

        return sprintf('%s->\'$.%s\'', $field, collect($path)->map(function ($part) {
            return '"'.$part.'"';
        })->implode('.'));
    }

    public function register()
    {
        $c = $this;

        Builder::macro('searchBy', function (array $search) use ($c) {
            $this->where(function () use (&$search, $c) {
                foreach ($search as $key => $value) {
                    if (preg_match('/^q_(.+)$/', $key, $match)) {
                        $this->where($c->unwrapColon($match[1]), 'like', "%$value%");
                    }
                }
            });

            return $this;
        });
        Builder::macro('searchByConvert', function (array $search) use ($c) {
            $this->where(function () use (&$search, $c) {
                foreach ($search as $key => $value) {
                    if (preg_match('/^q_(.+)$/', $key, $match)) {
                        $key = sprintf('convert(lower(%s) using utf8) like convert(lower(?) using utf8)', $c->wrapValue(
                          $c->unwrapColon($match[1])
                        ));
                        $this->whereRaw($key, ["%$value%"]);
                    }
                }
            });

            return $this;
        });

        Builder::macro('sortBy', function (array $sort) use ($c) {
            if (array_get($sort, 'by')) {
                $this->orderBy(array_get($c->unwrapColon($sort), 'by'), array_get($sort, 'direction'));
            }

            return $this;
        });

        Builder::macro('matchBy', function (array $data) use ($c) {
            foreach ($data as $key => $value) {
                if (preg_match('/^m_(.+)$/', $key, $match)) {
                    if (is_array($value)) {
                        $this->whereIn($c->unwrapColon($match[1]), $value);
                    } else {
                        $this->where($c->unwrapColon($match[1]), $value);
                    }
                }
            }

            return $this;
        });

        Builder::macro('inRange', function (array $data) use ($c) {
            foreach ($data as $key => $value) {
                if (preg_match('/^r_(.+)$/', $key, $match)) {
                    $column = $c->unwrapColon($match[1]);
                    $parts = explode('--', $value);
                    if ($parts[0] && $parts[1]) {
                        $this->whereBetween($column, $parts);
                    } elseif ($parts[0] && !$parts[1]) {
                        $this->where($column, '>', $parts[0]);
                    } elseif (!$parts[0] && $parts[1]) {
                        $this->where($column, '<=', $parts[1]);
                    }
                }
            }

            return $this;
        });

        Builder::macro('applyQuery', function (array $data, $convert = false) {
            $this->matchBy($data)
                ->inRange($data)
                ->sortBy($data);

            if ($convert) {
                $this->searchByConvert($data);
            } else {
                $this->searchBy($data);
            }
        });
    }
}
