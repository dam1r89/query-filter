This is set of Laravel macros which extend query builder to allow it simple conversion from 
URL query parameters to database query.


Query types are distinct by prefix and name of the column on which operator should be applied. 
For example `m_city=Belgrade` would match column `city` with value `Belgrade`. Prefix `m_` means exact match.


## Installation

This package is not published on packagist, so you need to add remote repository manually.

```json
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/dam1r89/query-filter.git"
    }
]
```

then type `composer require skillfactory/jet-query-filter`

## Explanation

It supports following types of query:

1. `q_<column_name>=value` is for `LIKE` operator
2. `m_<column_name>=value` is `=` operator
3. `r_<column_name>=<from>--<to>` check if column value is in given range. `from` or `to` are not required
4. `by=<column_name>&direction=[asc|desc]` sort by column name

Column name can have table prefix separated by colon (`:`)
  `m_<table_name:column_name>`

## Simple usage

To parse all arguments use this.

```php
Product::query()->applyQuery($request->input())
```

## More granular control

This will apply each helper macro one by one, same like this:

```php    
Product::query()
    ->matchBy($request->input()) // parse request parameters that start with `m_` prefix
    ->searchBy($request->input()) // -||- with `q_`
    ->inRange($request->input()) // -||- with `r_`
    ->sortBy($request->input()); // parse `by` and `direction` parameters
```

## Helper for querying JSON properties

Because MySql JSON property LIKE is not working like expected, there is additional operator `->searchByConvert`
which is converting to lowercase and removing nonstandard characters.

To use it with `applyQuery` add `true` as second parameter.


